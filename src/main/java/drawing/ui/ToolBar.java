package drawing.ui;

import drawing.commands.*;
import drawing.handlers.*;
import drawing.shapes.SimpleEdgeStrategy;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ToolBar extends HBox {

    private Button clearButton;
    private Button deleteButton;
    private Button groupButton;
    private Button ungroupButton;
    private Button rectangleButton;
    private Button circleButton;
    private Button triangleButton;
    private Button undoButton;
    private Button redoButton;
    private Button duplicateButton;
    private Button textButton;
    private Button edgeButton;
    private Button edgeStrategyButton;

    private final String[] edgeStrategies = new String[] {
            "SimpleEdge", "EdgeOrthogonal"
    };



    public ToolBar(DrawingPane drawingPane) throws IOException {
        clearButton = ButtonsFactory.createButton(ButtonsFactory.CLEAR, ButtonsFactory.ICONS_ONLY);
        clearButton.addEventFilter(ActionEvent.ACTION, new HandlerActionEvent(drawingPane, new ClearCommand(drawingPane)));

        deleteButton = ButtonsFactory.createButton(ButtonsFactory.DELETE, ButtonsFactory.ICONS_ONLY);
        deleteButton.addEventFilter(ActionEvent.ACTION, new HandlerActionEvent(drawingPane, new DeleteCommand(drawingPane)));

        groupButton = ButtonsFactory.createButton(ButtonsFactory.GROUP, ButtonsFactory.TEXT_ONLY);
        groupButton.addEventFilter(ActionEvent.ACTION, new HandlerActionEvent(drawingPane, new GroupCommand(drawingPane)));

        ungroupButton = ButtonsFactory.createButton(ButtonsFactory.UNGROUP, ButtonsFactory.TEXT_ONLY);
        ungroupButton.addEventFilter(ActionEvent.ACTION, new HandlerActionEvent(drawingPane, new UngroupCommand(drawingPane)));

        rectangleButton = ButtonsFactory.createButton(ButtonsFactory.RECTANGLE, ButtonsFactory.TEXT_ONLY);
        rectangleButton.addEventFilter(ActionEvent.ACTION, new RectangleButtonHandler(drawingPane));

        circleButton = ButtonsFactory.createButton(ButtonsFactory.CIRCLE, ButtonsFactory.TEXT_ONLY);
        circleButton.addEventFilter(ActionEvent.ACTION, new EllipseButtonHandler(drawingPane));

        triangleButton = ButtonsFactory.createButton(ButtonsFactory.TRIANGLE, ButtonsFactory.TEXT_ONLY);
        triangleButton.addEventFilter(ActionEvent.ACTION, new TriangleButtonHandler(drawingPane));

        duplicateButton = ButtonsFactory.createButton(ButtonsFactory.DUPLICATE, ButtonsFactory.TEXT_ONLY);
        duplicateButton.addEventFilter(ActionEvent.ACTION, new DuplicateButtonHandler(drawingPane, new DuplicateCommand(drawingPane)));

        textButton = ButtonsFactory.createButton(ButtonsFactory.TEXT, ButtonsFactory.TEXT_ONLY);
        textButton.addEventFilter(ActionEvent.ACTION, new TextButtonHandler(drawingPane, new TextCommand(drawingPane)));

        edgeButton = ButtonsFactory.createButton(ButtonsFactory.EDGE, ButtonsFactory.TEXT_ONLY);
        edgeButton.addEventFilter(ActionEvent.ACTION, new EdgeButtonHandler(drawingPane, new EdgeCommand(drawingPane)));

        undoButton = ButtonsFactory.createButton(ButtonsFactory.UNDO, ButtonsFactory.TEXT_ONLY);
        undoButton.addEventFilter(ActionEvent.ACTION, new UndoButtonHandler(drawingPane, new UndoCommand(drawingPane)));

        redoButton = ButtonsFactory.createButton(ButtonsFactory.REDO, ButtonsFactory.TEXT_ONLY);
        redoButton.addEventFilter(ActionEvent.ACTION, new RedoButtonHandler(drawingPane, new RedoCommand(drawingPane)));

        ComboBox comboBox = new ComboBox();
        for (String strategyName : edgeStrategies) {
            comboBox.getItems().add(strategyName);
        }
        comboBox.getSelectionModel().select(0);

        edgeStrategyButton = ButtonsFactory.createButton(ButtonsFactory.EDGE_STRATEGY, ButtonsFactory.TEXT_ONLY);
        edgeStrategyButton.addEventFilter(ActionEvent.ACTION, new EdgeStrategyButtonHandler(drawingPane, new EdgeStrategyCommand(drawingPane, (String) comboBox.getSelectionModel().getSelectedItem())));


        this.getChildren().addAll(clearButton, deleteButton, groupButton, ungroupButton, rectangleButton, circleButton, triangleButton, textButton, duplicateButton, undoButton, redoButton, edgeButton, comboBox, edgeStrategyButton);
        this.setPadding(new Insets(5));
        this.setSpacing(5.0);
        this.getStyleClass().add("toolbar");
    }

    public String[] getEdgeStrategies() {
        return this.edgeStrategies;
    }

}
