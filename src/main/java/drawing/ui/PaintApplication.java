package drawing.ui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.tools.Tool;

/**
 * Created by lewandowski on 20/12/2017.
 */
public class PaintApplication extends Application {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private Scene scene;
    private BorderPane root;
    private DrawingPane drawingPane;
    private StatutBar statutBar;
    private ExceptionBar exceptionBar;

    @Override
    public void start(Stage primaryStage) throws Exception {
        root = new BorderPane();
        scene = new Scene(root, WIDTH, HEIGHT);

        root.getStylesheets().add(
                PaintApplication.class.getClassLoader().getResource("style/Paint.css").toExternalForm());

        drawingPane = new DrawingPane();
        drawingPane.getStyleClass().add("drawingPane");
        root.setCenter(drawingPane);

        ToolBar toolBar = new ToolBar(drawingPane);
        root.setTop(toolBar);

        statutBar = new StatutBar(drawingPane);
        drawingPane.addObserver(statutBar);
        drawingPane.getSelection().addObserver(statutBar);
        statutBar.getStyleClass().add("statutbar");

        exceptionBar = new ExceptionBar(drawingPane);
        drawingPane.getCommandHistory().addObserver(exceptionBar);

        VBox bottomBar = new VBox(statutBar, exceptionBar);
        root.setBottom(bottomBar);

        primaryStage.setTitle("Drawing");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public DrawingPane getDrawingPane() {
        return drawingPane;
    }

    public StatutBar getStatutBar() {
        return statutBar;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
