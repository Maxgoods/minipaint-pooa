package drawing.ui;

import drawing.Observer;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class ExceptionBar extends HBox implements Observer {

    private Label label;
    private DrawingPane dpane;

    public ExceptionBar(DrawingPane dpane) {
        this.dpane = dpane;
        this.label = new Label("");
        this.getChildren().add(this.label);
    }

    @Override
    public void update() {
        Exception e = this.dpane.getCommandHistory().getCurrentException();
        label.setText(e.getMessage());
    }

    public String getLabelText() {
        return label.getText();
    }
}
