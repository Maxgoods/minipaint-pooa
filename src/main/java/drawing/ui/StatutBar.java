package drawing.ui;

import drawing.Observer;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class StatutBar extends HBox implements Observer {

    private Label label;
    private DrawingPane dpane;

    public StatutBar(DrawingPane dpane) {
        this.dpane = dpane;
        this.label = new Label("0 forme(s) - 0 forme(s) selectionnée(s)");
        this.getChildren().add(this.label);
    }

    @Override
    public void update() {
        label.setText(dpane.getNbShapes() + " forme(s) - " + dpane.getSelection().getNbShapesSelected()  +" forme(s) selectionnée(s)");
    }

    public String getLabelText() {
        return label.getText();
    }

}
