package drawing.commands;

import drawing.shapes.*;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class EdgeCommand implements ICommand {
    private DrawingPane drawingPane;
    private Edge undoEdge;

    public EdgeCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }


    @Override
    public void execute() throws Exception {
        IShape from = null;
        IShape to = null;

        for (IShape shape: this.drawingPane.getSelection()) {
            if (from != null && to != null) {
                throw new Exception("You need to select 2 shapes");
            }
            if (from == null) {
                from = shape;
                continue;
            }
            if (to == null) {
                to = shape;
                continue;
            }
        }

        if (from == null || to == null) {
            throw new Exception("You need to select 2 shapes");
        }

        IEdgeStrategy strategy = new SimpleEdgeStrategy();
        this.undoEdge = new Edge(strategy, from, to);
        drawingPane.addShape(undoEdge);

        this.drawingPane.getSelection().clearSelection();
    }

    @Override
    public void undo() {
        drawingPane.removeShape(undoEdge);
    }

    @Override
    public ICommand clone() {
        return new EdgeCommand(drawingPane);
    }
}
