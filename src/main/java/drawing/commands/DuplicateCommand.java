package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class DuplicateCommand implements ICommand {
    private DrawingPane drawingPane;
    private List<IShape> shapes;

    public DuplicateCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        shapes = new ArrayList<IShape>();

        for (IShape shape: this.drawingPane.getSelection()) {
            IShape newShape = shape.clone();

            shapes.add(newShape);
            this.drawingPane.addShape(newShape);
        }

        this.drawingPane.getSelection().clearSelection();
    }

    @Override
    public void undo() {
        for (IShape shape: this.shapes) {
            this.drawingPane.removeShape(shape);
        }
    }

    @Override
    public ICommand clone() {
        return new DuplicateCommand(this.drawingPane);
    }
}
