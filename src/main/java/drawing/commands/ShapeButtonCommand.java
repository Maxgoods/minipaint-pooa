package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class ShapeButtonCommand implements ICommand {
    private DrawingPane drawingPane;
    private IShape shape;


    public ShapeButtonCommand(DrawingPane drawingPane, IShape shape) {
        this.drawingPane = drawingPane;
        this.shape = shape;
    }

    @Override
    public void execute() {
        drawingPane.addShape(shape);
    }

    @Override
    public void undo() {
        drawingPane.removeShape(shape);
    }

    @Override
    public ICommand clone() {
        return new ShapeButtonCommand(drawingPane, shape);
    }

}
