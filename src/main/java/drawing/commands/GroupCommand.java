package drawing.commands;

import drawing.shapes.IShape;
import drawing.shapes.ShapeGroup;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class GroupCommand implements ICommand {
    private DrawingPane drawingPane;
    private ShapeGroup undoShapeGroup;

    public GroupCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        ShapeGroup shapeGroup = new ShapeGroup();
        for (IShape shape: this.drawingPane.getSelection()) {
            shapeGroup.add(shape);
            drawingPane.removeShape(shape);
        }

        this.drawingPane.getSelection().clearSelection();

        drawingPane.addShape(shapeGroup);

        undoShapeGroup = shapeGroup;
    }

    @Override
    public void undo() {
        if (undoShapeGroup == null) {
            return;
        }

        drawingPane.removeShape(undoShapeGroup);
        for (IShape s : undoShapeGroup) {
            drawingPane.addShape(s);
        }
    }

    @Override
    public ICommand clone() {
        return new GroupCommand(drawingPane);
    }
}
