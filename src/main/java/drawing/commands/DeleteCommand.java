package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class DeleteCommand implements ICommand {

    private DrawingPane drawingPane;
    private List<IShape> shapes  = new ArrayList<>();

    public DeleteCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        shapes = new ArrayList<IShape>();

        for (IShape shape: this.drawingPane.getSelection()) {
            shapes.add(shape);
            this.drawingPane.removeShape(shape);
        }

        this.drawingPane.getSelection().clearSelection();
    }

    @Override
    public void undo() {
        for (IShape s : shapes) {
            this.drawingPane.addShape(s);
        }
    }

    @Override
    public ICommand clone() {
        return new DeleteCommand(drawingPane);
    }
}
