package drawing.commands;

import drawing.shapes.IShape;
import drawing.shapes.TextDecorator;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class TextCommand implements ICommand {
    private DrawingPane drawingPane;
    private List<TextDecorator> undoTextDecorator = new ArrayList<TextDecorator>();
    private List<IShape> undoShape = new ArrayList<>();

    public TextCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() throws Exception {
        for (IShape shape: this.drawingPane.getSelection()) {
            try {
                TextDecorator textDecorator = new TextDecorator("Text", shape);

                undoTextDecorator.add(textDecorator);
                undoShape.add(shape);

                drawingPane.removeShape(shape);
                drawingPane.addShape(textDecorator);
            } catch (Exception e) {
                throw e;
            }

        }

        this.drawingPane.getSelection().clearSelection();
    }

    @Override
    public void undo() {
        for (TextDecorator textDecorator : undoTextDecorator) {
            drawingPane.removeShape(textDecorator);
        }
        for (IShape shape : undoShape) {
            drawingPane.addShape(shape);
        }
    }

    @Override
    public ICommand clone() {
        return new TextCommand(this.drawingPane);
    }
}

