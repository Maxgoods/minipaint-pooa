package drawing.commands;

import drawing.shapes.*;
import drawing.ui.DrawingPane;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EdgeStrategyCommand implements ICommand{
    private DrawingPane drawingPane;
    private String selectedStrategy;
    Map<IShape, IEdgeStrategy> undoEdge = new HashMap<IShape, IEdgeStrategy>();

    public EdgeStrategyCommand(DrawingPane drawingPane, String selectedStrategy) {
        this.drawingPane = drawingPane;
        this.selectedStrategy = selectedStrategy;
    }

    @Override
    public void execute() throws Exception {
        IEdgeStrategy strategy;
        if (selectedStrategy.equals("SimpleEdge")) {
            strategy = new SimpleEdgeStrategy();
        } else {
            strategy = new EdgeOrthogonalStrategy();
        }

        for (IShape shape: this.drawingPane.getSelection()) {
            if (shape instanceof Edge) {
                undoEdge.put(shape, ((Edge) shape).getStrategy());
                ((Edge) shape).setEdgeStrategy(strategy);
            }
        }
    }

    @Override
    public void undo() {
        Set<IShape> edges = undoEdge.keySet();
        for (IShape shape: edges) {
            IEdgeStrategy strategy = undoEdge.get(shape);
            ((Edge) shape).setEdgeStrategy(strategy);
        }
    }

    @Override
    public ICommand clone() {
        return new EdgeStrategyCommand(drawingPane, selectedStrategy);
    }
}
