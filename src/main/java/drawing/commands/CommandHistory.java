package drawing.commands;

import drawing.Observer;

import java.util.ArrayList;
import java.util.List;

public class CommandHistory {
    private int lastCommandIndex = -1;
    private List<ICommand> commands = new ArrayList<ICommand>();
    private List<Observer> observers  = new ArrayList<Observer>();
    private Exception currentException;

    public void exec(ICommand command) {
        ICommand executedCommand = command.clone();

        try {
            executedCommand.execute();
            commands.add(executedCommand);
            this.lastCommandIndex++;
        } catch (Exception e) {
            currentException = e;
            this.notifyAllObservers();
        }

    }

    public void undo() {
        if (this.lastCommandIndex < 0) {
            return;
        }
        ICommand lastCommand = commands.get(this.lastCommandIndex);
        lastCommand.undo();
        this.lastCommandIndex--;
        removeElementsAfterIndex();
    }

    public void redo() {
        if ((this.lastCommandIndex + 1) >= this.commands.size()) {
            return;
        }
        ICommand lastCommand = commands.get(this.lastCommandIndex + 1);

        try {
            lastCommand.execute();
            this.lastCommandIndex++;
        } catch (Exception e) {
            currentException = e;
            this.notifyAllObservers();
        }

    }

    private void removeElementsAfterIndex() {
        int nbCommandsTotal = this.commands.size();
        System.out.println(lastCommandIndex);
        for (int i = nbCommandsTotal; i <= this.lastCommandIndex + 1; i--) {
            System.out.println(i);
            System.out.println(nbCommandsTotal);
            this.commands.remove(i - 1);
        }
    }

    public void addObserver(Observer o) {
        observers.add(o);
    }

    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    private void notifyAllObservers() {
        for (Observer o: observers)
            o.update();
    }

    public Exception getCurrentException() {
        return this.currentException;
    }

}
