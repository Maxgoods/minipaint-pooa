package drawing.commands;

import drawing.ui.DrawingPane;

public class RedoCommand implements ICommand  {

    private DrawingPane drawingPane;

    public RedoCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        this.drawingPane.getCommandHistory().redo();
    }

    @Override
    public void undo() {
        // Useless ?
    }

    @Override
    public ICommand clone() {
        return new RedoCommand(drawingPane);
    }
}
