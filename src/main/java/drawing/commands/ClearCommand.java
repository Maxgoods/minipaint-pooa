package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;

import java.util.ArrayList;
import java.util.List;

public class ClearCommand implements ICommand {

    private DrawingPane drawingPane;
    private List<IShape> shapes;

    public ClearCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        shapes = new ArrayList<IShape>();
        for (IShape s : this.drawingPane) {
            shapes.add(s);
        }

        this.drawingPane.clear();
    }

    @Override
    public void undo() {
        for (IShape s : shapes) {
            this.drawingPane.addShape(s);
        }
    }

    @Override
    public ICommand clone() {
     return new ClearCommand(drawingPane);
    }

}
