package drawing.commands;

import drawing.shapes.IShape;
import drawing.shapes.ShapeGroup;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class UngroupCommand implements ICommand {
    private DrawingPane drawingPane;
    private List<IShape> shapes = new ArrayList<>();

    public UngroupCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        for (IShape shape: this.drawingPane.getSelection()) {
            if (shape instanceof ShapeGroup) {
                ShapeGroup shapeGroup = (ShapeGroup) shape;
                drawingPane.removeShape(shapeGroup);

                for (IShape s : shapeGroup) {
                    drawingPane.addShape(s);
                    this.shapes.add(s);
                }
            }

        }
        this.drawingPane.getSelection().clearSelection();
    }

    @Override
    public void undo() {
        ShapeGroup shapeGroup = new ShapeGroup();
        for (IShape shape: this.shapes) {
            shapeGroup.add(shape);
            drawingPane.removeShape(shape);
        }

        this.drawingPane.getSelection().clearSelection();

        drawingPane.addShape(shapeGroup);
    }

    @Override
    public ICommand clone() {
        return new UngroupCommand(drawingPane);
    }
}
