package drawing.commands;

import drawing.shapes.IShape;
import drawing.ui.DrawingPane;

import java.util.ArrayList;
import java.util.List;

public class UndoCommand implements ICommand {
    private DrawingPane drawingPane;

    public UndoCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        this.drawingPane.getCommandHistory().undo();
    }

    @Override
    public void undo() {
       // Useless ?
    }

    @Override
    public ICommand clone() {
        return new UndoCommand(drawingPane);
    }

}
