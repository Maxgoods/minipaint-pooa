package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class TextDecorator implements IShape {

    private IShape shape;
    private Text text;

    public TextDecorator(String txt, IShape shape) throws Exception {
        if (shape instanceof ShapeGroup) {
            throw new Exception("Can't use TextDecorator with ShapeGroup");
        }
        this.shape = shape;
        this.text = new Text(txt);

        this.text.translateXProperty().bind(shape.translateXProperty());
        this.text.translateYProperty().bind(shape.translateYProperty());
    }

    @Override
    public boolean isSelected() {
        return shape.isSelected();
    }

    @Override
    public void setSelected(boolean selected) {
        shape.setSelected(selected);
    }

    @Override
    public boolean isOn(double x, double y) {
        return shape.isOn(x, y);
    }


    @Override
    public void offset(double x, double y) {
        shape.offset(x, y);
    }

    @Override
    public void addShapeToPane(Pane pane) {
        shape.addShapeToPane(pane);
        pane.getChildren().add(text);
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        shape.removeShapeFromPane(pane);
        pane.getChildren().remove(text);
    }


    @Override
    public IShape clone() {
        TextDecorator textDecorator = null;
        try {
            textDecorator = new TextDecorator(text.getText(), shape.clone());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return textDecorator;
    }

    @Override
    public ObservableValue translateXProperty() {
        return this.shape.translateXProperty();
    }

    @Override
    public ObservableValue translateYProperty() {
        return this.shape.translateYProperty();
    }
}
