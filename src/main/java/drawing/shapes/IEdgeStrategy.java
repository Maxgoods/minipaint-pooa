package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.shape.Path;

public interface IEdgeStrategy {
    void buildPath(IShape from, IShape to, Path path);
    public ObservableValue middleXProperty();
    public ObservableValue middleYProperty();


}
