package drawing.shapes;

import javafx.beans.binding.Binding;
import javafx.beans.value.ObservableValue;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class EdgeOrthogonalStrategy implements IEdgeStrategy{
    @Override
    public void buildPath(IShape from, IShape to, Path path) {
        path.getElements().clear();

        MoveTo moveBottom = new MoveTo();
        moveBottom.xProperty().bind(from.translateXProperty());
        moveBottom.yProperty().bind(from.translateYProperty());
        path.getElements().add(moveBottom);

        LineTo lineBottom = new LineTo();
        lineBottom.xProperty().bind(to.translateXProperty());
        lineBottom.yProperty().bind(to.translateYProperty());
        path.getElements().add(lineBottom);

    }

    @Override
    public ObservableValue middleXProperty() {
        return null;
    }

    @Override
    public ObservableValue middleYProperty() {
        return null;
    }
}
