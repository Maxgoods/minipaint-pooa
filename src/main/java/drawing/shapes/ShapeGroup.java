package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShapeGroup implements IShape, Iterable<IShape> {

    private List<IShape> shapes = new ArrayList<>();

    public void ShapeAdapter() {

    }

    public void add(IShape shape) {
        shapes.add(shape);
    }

    public void remove(IShape shape) {
        shapes.remove(shape);
    }

    @Override
    public boolean isSelected() {
        boolean isSelected = false;
        for (IShape s : shapes) {
            if (s.isSelected()) {
                isSelected = true;
                break;
            }
        }

        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        for (IShape s : shapes) {
            s.setSelected(selected);
        }
    }

    @Override
    public boolean isOn(double x, double y) {
        boolean isOn = false;
        for (IShape s : shapes) {
            if (s.isOn(x, y)) {
                isOn = true;
                break;
            }
        }

        return isOn;
    }

    @Override
    public void offset(double x, double y) {
        for (IShape s : shapes) {
            s.offset(x, y);
        }
    }

    @Override
    public void addShapeToPane(Pane pane) {
        for (IShape s : shapes) {
            s.addShapeToPane(pane);
        }
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        for (IShape s : shapes) {
            s.removeShapeFromPane(pane);
        }
    }

    @Override
    public Iterator<IShape> iterator() {
        return shapes.iterator();
    }

    @Override
    public IShape clone() {
        ShapeGroup newShapeGroup = new ShapeGroup();
        for (IShape shape : this.shapes) {
            IShape newShape = shape.clone();
            newShapeGroup.add(newShape);
        }

        return newShapeGroup;
    }

    @Override
    public ObservableValue translateXProperty() {
        return null;
    }

    @Override
    public ObservableValue translateYProperty() {
        return null;
    }

}
