package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class SimpleEdgeStrategy implements IEdgeStrategy {
    @Override
    public void buildPath(IShape from, IShape to, Path path) {
        path.getElements().clear();

        MoveTo moveTo = new MoveTo();
        moveTo.xProperty().bind(from.translateXProperty());
        moveTo.yProperty().bind(from.translateYProperty());

        LineTo lineTo = new LineTo();
        lineTo.xProperty().bind(to.translateXProperty());
        lineTo.yProperty().bind(to.translateYProperty());

        path.getElements().add(moveTo);
        path.getElements().add(lineTo);
    }

    @Override
    public ObservableValue middleXProperty() {
        return null;
    }

    @Override
    public ObservableValue middleYProperty() {
        return null;
    }
}
