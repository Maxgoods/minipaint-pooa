package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import javafx.scene.shape.*;

public class Edge implements IShape {
    private IEdgeStrategy strategy;
    private IShape from;
    private IShape to;
    private Path shape = new Path();

    public Edge (IEdgeStrategy strategy, IShape from, IShape to) {
        this.from = from;
        this.to = to;
        this.strategy = strategy;

        this.strategy.buildPath(this.from, this.to, this.shape);
    }

    @Override
    public boolean isSelected() {
        return shape.getStyleClass().contains("selected");
    }

    @Override
    public void setSelected(boolean selected) {
        if (selected)
            shape.getStyleClass().add("selected");
        else
            shape.getStyleClass().remove("selected");
    }

    @Override
    public boolean isOn(double x, double y) {
        return shape.getBoundsInParent().contains(x, y);
    }

    @Override
    public void offset(double x, double y) {
        shape.setTranslateX(shape.getTranslateX() + x);
        shape.setTranslateY(shape.getTranslateY() + y);
    }

    @Override
    public void addShapeToPane(Pane pane) {
        pane.getChildren().add(shape);
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        pane.getChildren().remove(shape);
    }


    @Override
    public IShape clone() {
        return new Edge(strategy, from, to);
    }

    @Override
    public ObservableValue translateXProperty() {
        return null;
    }

    @Override
    public ObservableValue translateYProperty() {
        return null;
    }

    public IEdgeStrategy getStrategy() {
        return this.strategy;
    }

    public void setEdgeStrategy(IEdgeStrategy strategy) {
        this.strategy = strategy;
        this.strategy.buildPath(this.from, this.to, this.shape);
    }
}
