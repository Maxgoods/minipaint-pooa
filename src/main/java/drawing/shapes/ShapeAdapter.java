package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

public class ShapeAdapter implements IShape {

    private Shape shape;
    private double initialX;
    private double initialY;

    public ShapeAdapter(Shape shape) {
        this.shape = shape;
        this.initialX = shape.getBoundsInParent().getMinX() + (shape.getBoundsInParent().getWidth() / 2);
        this.initialY = shape.getBoundsInParent().getMinY() + (shape.getBoundsInParent().getHeight() / 2);
    }

    @Override
    public boolean isSelected() {
        return shape.getStyleClass().contains("selected");
    }

    @Override
    public void setSelected(boolean selected) {
        if (selected)
            shape.getStyleClass().add("selected");
        else
            shape.getStyleClass().remove("selected");
    }

    @Override
    public boolean isOn(double x, double y) {
        return shape.getBoundsInParent().contains(x, y);
    }

    @Override
    public void offset(double x, double y) {
        shape.setTranslateX(shape.getTranslateX() + x);
        shape.setTranslateY(shape.getTranslateY() + y);
    }

    @Override
    public void addShapeToPane(Pane pane) {
        pane.getChildren().add(shape);
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        pane.getChildren().remove(shape);
    }

    @Override
    public IShape clone() {
        Shape newShape = Shape.union(this.shape, this.shape);
        newShape.getStyleClass().addAll(this.shape.getStyleClass());
        ShapeAdapter newShapeAdapter =  new ShapeAdapter(newShape);
        newShapeAdapter.setSelected(false);

        return newShapeAdapter;
    }

    @Override
    public ObservableValue translateXProperty() {
        return this.shape.translateXProperty().add(this.initialX);
    }

    @Override
    public ObservableValue translateYProperty() {
        return this.shape.translateYProperty().add(this.initialY);
    }
}
