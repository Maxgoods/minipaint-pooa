package drawing.handlers;

import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class UndoButtonHandler implements EventHandler<ActionEvent> {

    private DrawingPane drawingPane;
    private ICommand command;

    public UndoButtonHandler(DrawingPane drawingPane, ICommand command) {
        this.drawingPane = drawingPane;
        this.command = command;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            this.command.execute();
        } catch (Exception e) {

        }

    }
}
