package drawing.handlers;

import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class TextButtonHandler  implements EventHandler<ActionEvent> {

    private DrawingPane drawingPane;
    private ICommand command;

    public TextButtonHandler(DrawingPane drawingPane, ICommand command) {
        this.drawingPane = drawingPane;
        this.command = command;
    }

    @Override
    public void handle(ActionEvent event) {
        this.drawingPane.getCommandHistory().exec(this.command);
    }
}