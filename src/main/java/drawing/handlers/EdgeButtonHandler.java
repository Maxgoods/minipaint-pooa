package drawing.handlers;

import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class EdgeButtonHandler implements EventHandler<ActionEvent> {

    private DrawingPane drawingPane;
    private ICommand command;

    public EdgeButtonHandler(DrawingPane drawingPane, ICommand command) {
        this.drawingPane = drawingPane;
        this.command = command;
    }

    @Override
    public void handle(ActionEvent event) {
        this.drawingPane.getCommandHistory().exec(this.command);
    }
}