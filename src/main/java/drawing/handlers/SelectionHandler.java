package drawing.handlers;

import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import drawing.shapes.IShape;
import drawing.Observer;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;
import java.util.Iterator;

public class SelectionHandler implements EventHandler<MouseEvent>, Iterable<IShape>{

    private DrawingPane drawingPane;
    private ArrayList<IShape> selectedShapes;
    private ArrayList<Observer> observers;

    public SelectionHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.drawingPane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        this.selectedShapes = new ArrayList<>();
        this.observers = new ArrayList<>();
    }
    @Override
    public void handle(MouseEvent event) {
        IShape isOnShape = null;
        for (IShape s: drawingPane)
            if (s.isOn(event.getX(), event.getY())) {
                isOnShape = s;
                break;
            }

        if (isOnShape != null) {
            if (selectedShapes.contains(isOnShape)) {
                if (event.isShiftDown() && selectedShapes.size() > 1)
                    removeFromSelection(isOnShape);
            } else {
                if (!event.isShiftDown()) {
                    clearSelection();
                }
                addToSelection(isOnShape);
            }
        } else {
            clearSelection();
        }
    }

    protected void addToSelection(IShape shape) {
        if (selectedShapes.contains(shape))
            return;
        shape.setSelected(true);
        selectedShapes.add(shape);
        notifyAllObservers();
    }

    protected void removeFromSelection(IShape shape) {
        if (!selectedShapes.contains(shape))
            return;
        shape.setSelected(false);
        selectedShapes.remove(shape);
        notifyAllObservers();
    }

    public void clearSelection() {
        selectedShapes.forEach(iShape -> iShape.setSelected(false));
        selectedShapes.clear();
        notifyAllObservers();
    }

    @Override
    public Iterator<IShape> iterator() {
        return selectedShapes.iterator();
    }

    public int getNbShapesSelected() {
        return selectedShapes.size();
    }

    public void addObserver(Observer o) {
        observers.add(o);
    }

    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    private void notifyAllObservers() {
        for (Observer o: observers)
            o.update();
    }
}
