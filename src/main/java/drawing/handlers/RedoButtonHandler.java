package drawing.handlers;

import drawing.commands.ICommand;
import drawing.ui.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.concurrent.ExecutionException;

public class RedoButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    private ICommand command;

    public RedoButtonHandler(DrawingPane drawingPane, ICommand command) {
        this.drawingPane = drawingPane;
        this.command = command;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            this.command.execute();
        } catch (Exception e) {

        }

    }
}
