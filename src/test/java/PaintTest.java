import drawing.shapes.IShape;
import drawing.ui.ButtonsFactory;
import drawing.ui.PaintApplication;
import drawing.shapes.ShapeAdapter;
import drawing.ui.StatutBar;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.util.Iterator;

import static org.junit.Assert.*;

public class PaintTest extends ApplicationTest {

    PaintApplication app;

    private Shape createShape (String shapeType) {
        int x = 20 + getNbShapes() * 150;
        int y = 20 + (int) (200 * Math.random());
        int w = 50 + (int) (70 * Math.random());
        clickOn(shapeType);
        moveTo(app.getDrawingPane())
                .moveBy(-app.WIDTH / 2 + x, -app.HEIGHT / 2 + y)
                .drag().dropBy(w, w);

        int index = app.getDrawingPane().getNbShapes();
        if (index > 0) {
            index--;
        }
        return (Shape) app.getDrawingPane().getChildren().get(app.getDrawingPane().getChildren().size() - 1);
    }

    private int getNbShapes() {
        int nb = 0;
        for (IShape s : app.getDrawingPane()) {
            nb++;
        }
        return nb;
    }

    private int getNbShapesSelected() {
        int nb = 0;
        for (IShape s : app.getDrawingPane().getSelection()) {
            nb++;
        }
        return nb;
    }

    private void clickOnButton(String element) {
        clickOn("#" + element);
    }

    @Override
    public void start(Stage stage) {
        try {
            app = new PaintApplication();
            app.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_draw_circle_programmatically() {
        IShape shape = new ShapeAdapter(new Ellipse(20, 20, 30, 30));
        interact(() -> {
                    app.getDrawingPane().addShape(shape);
                });
        Iterator it = app.getDrawingPane().getChildren().iterator();
        assertTrue(it.next() instanceof Ellipse);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_circle() {
        // given:
        clickOnButton(ButtonsFactory.CIRCLE);
        moveBy(60,60);

        // when:
        drag().dropBy(30,30);
        //press(MouseButton.PRIMARY); moveBy(30,30); release(MouseButton.PRIMARY);

        // then:
        Iterator it = app.getDrawingPane().getChildren().iterator();
        assertTrue(it.next() instanceof Ellipse);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_rectangle() {
        // given:
        clickOnButton(ButtonsFactory.RECTANGLE);
        moveBy(0,60);

        // when:
        drag().dropBy(70,40);

        // then:
        Iterator it = app.getDrawingPane().getChildren().iterator();
        assertTrue(it.next() instanceof Rectangle);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_clear() {
        // given:
        clickOnButton(ButtonsFactory.RECTANGLE);
        moveBy(30,60).drag().dropBy(70,40);
        clickOnButton(ButtonsFactory.CIRCLE);
        moveBy(-30,160).drag().dropBy(70,40);

        // when:
        clickOnButton(ButtonsFactory.CLEAR);

        // then:
        assertFalse(app.getDrawingPane().iterator().hasNext());
    }

    @Test
    public void should_draw_triangle() {
        // given:
        clickOnButton(ButtonsFactory.TRIANGLE);
        moveBy(0,60);

        // when:
        drag().dropBy(70,40);

        // then:
        Iterator it = app.getDrawingPane().getChildren().iterator();
        assertTrue(it.next() instanceof Polygon);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_triangle_programmatically() {
        IShape shape = new ShapeAdapter(new Polygon(50, 0, 0, 100, 100, 100));
        interact(() -> {
            app.getDrawingPane().addShape(shape);
        });
        Iterator it = app.getDrawingPane().getChildren().iterator();
        assertTrue(it.next() instanceof Polygon);
        assertFalse(it.hasNext());
    }


    @Test
    public void should_update_statutBar_when_create_shape() {
        // given:
        StatutBar statutBar = app.getStatutBar();
        String defaultLabelText = statutBar.getLabelText();

        clickOnButton(ButtonsFactory.TRIANGLE);
        moveBy(0,60);

        // when:
        drag().dropBy(70,40);

        // then:
        String endLabelText = statutBar.getLabelText();
        assertFalse(defaultLabelText.equals(endLabelText));
        assertTrue(endLabelText.equals("1 forme(s) - 0 forme(s) selectionnée(s)"));
    }

    @Test
    public void should_select_shape() {
        Shape s1 = createShape(ButtonsFactory.RECTANGLE);
        Shape s2 = createShape(ButtonsFactory.CIRCLE);

        assertEquals(2, getNbShapes());
        assertEquals(0, getNbShapesSelected());

        clickOn(s1);
        assertEquals(1, getNbShapesSelected());

        press(KeyCode.SHIFT).moveTo(s2).clickOn(s2);
        release(KeyCode.SHIFT);
        assertEquals(2, getNbShapesSelected());
    }

    @Test
    public void should_update_statutBar_when_select_shape() {
        // given:
        StatutBar statutBar = app.getStatutBar();
        String defaultLabelText = statutBar.getLabelText();
        Shape s1 = createShape(ButtonsFactory.RECTANGLE);

        // when:
        clickOn(s1);

        // then:
        String endLabelText = statutBar.getLabelText();
        assertFalse(defaultLabelText.equals(endLabelText));
        assertTrue(endLabelText.equals("1 forme(s) - 1 forme(s) selectionnée(s)"));
    }

    @Test
    public void should_delete_shape() {
        // given:
        Shape s1 = createShape(ButtonsFactory.RECTANGLE);
        assertEquals(1, getNbShapes());

        // when:
        clickOn(s1);
        assertEquals(1, getNbShapesSelected());
        clickOnButton(ButtonsFactory.DELETE);

        // then:
        assertEquals(0, getNbShapes());
        assertEquals(0, getNbShapesSelected());
    }

    @Test
    public void should_create_group() {
        Shape s1 = createShape(ButtonsFactory.RECTANGLE);
        Shape s2 = createShape(ButtonsFactory.RECTANGLE);

        clickOn(s1);
        press(KeyCode.SHIFT).moveTo(s2).clickOn(s2);
        release(KeyCode.SHIFT);

        assertEquals(2, getNbShapes());

        Shape shapeGroup = createShape(ButtonsFactory.GROUP);
        assertEquals(1, getNbShapes());
    }

    @Test
    public void should_delete_group() {
        Shape s1 = createShape(ButtonsFactory.RECTANGLE);
        Shape s2 = createShape(ButtonsFactory.RECTANGLE);

        clickOn(s1);
        press(KeyCode.SHIFT).moveTo(s2).clickOn(s2);
        release(KeyCode.SHIFT);

        Shape shapeGroup = createShape(ButtonsFactory.GROUP);
        assertEquals(1, getNbShapes());

        clickOn(shapeGroup);
        clickOnButton(ButtonsFactory.UNGROUP);
        assertEquals(2, getNbShapes());
    }

    @Test
    public void should_undo_create_shape() {
        Shape s1 = createShape(ButtonsFactory.CIRCLE);
        assertEquals(1, getNbShapes());

        clickOnButton(ButtonsFactory.UNDO);

        assertEquals(0, getNbShapes());
    }

    @Test
    public void should_undo_three_times() {
        Shape s1 = createShape(ButtonsFactory.CIRCLE);
        clickOnButton(ButtonsFactory.CLEAR);

        Shape s2 = createShape(ButtonsFactory.RECTANGLE);
        clickOnButton(ButtonsFactory.CLEAR);

        clickOnButton(ButtonsFactory.UNDO);
        clickOnButton(ButtonsFactory.UNDO);
        clickOnButton(ButtonsFactory.UNDO);

        assertEquals(1, getNbShapes());
        Iterator it = app.getDrawingPane().getChildren().iterator();
        assertTrue(it.next() instanceof Ellipse);
    }

    @Test
    public void should_redo() {
        Shape s1 = createShape(ButtonsFactory.CIRCLE);

        assertEquals(1, getNbShapes());
        clickOnButton(ButtonsFactory.UNDO);
        assertEquals(0, getNbShapes());

        clickOnButton(ButtonsFactory.REDO);
        assertEquals(1, getNbShapes());
    }

    @Test
    public void should_duplicate_shape() {
        Shape s1 = createShape(ButtonsFactory.CIRCLE);

        clickOn(s1);
        clickOnButton(ButtonsFactory.DUPLICATE);
        assertEquals(2, getNbShapes());
    }

}